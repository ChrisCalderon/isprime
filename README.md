# isprime

A small program that checks if a number is prime. Only integers that fit in 64 bits are supported. To build, do the standard `./configure && make`.

This is the help output:
```
Usage: isprime N
       isprime --version
       isprime ( -h | --help )

Checks if N is a prime number.
If N is a prime, 0 is returned.
If N is not a prime, 1 is returned.
If N is a negative integer then isprime checks 2^64 + N.

Options:
--version       Prints version info and exits.
-h, --help      Prints this help info and exits.
```

Here are some results from running the benchmark on my machines (checking if the number 2^61 - 1 is prime):

| CPU | OS | Compiler | Runtime |
|-----|----|----------|---------|
| Apple M1, 3.2GHz, 8 cores, 8 threads | macOS 11.1 | Apple clang 12.0.0 |  0.090 seconds |
| Intel Xeon W-3245, 3.2GHz, 16 cores, 32 threads | macOS 10.15.7 | Apple clang 12.0.0 | 0.130 seconds |
| IBM 970MP (a.k.a. G5), 2.5GHz, 2 cores, 2 threads | Mac OS X 10.5.8 | Apple GCC 4.2 |  7.810 seconds |
| Motorola MPC7450 (a.k.a. G4), 1.42GHz, 2 CPUs, 2 threads | Mac OS X 10.5.8 | Apple GCC 4.2 | 22.510 seconds |
