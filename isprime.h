/*
    This file is part of isprime.

    isprime is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    isprime is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with isprime.  If not, see <https://www.gnu.org/licenses/>.

    To contact the author, send an email to calderonchristian73 AT gmail DOT com.
*/
#ifndef ISPRIME_H
#define ISPRIME_H

#include <pthread.h>
#include <unistd.h>
#include <inttypes.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>

bool isprime(uint64_t n);

#endif
