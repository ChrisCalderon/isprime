/*
    This file is part of isprime.

    isprime is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    isprime is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with isprime.  If not, see <https://www.gnu.org/licenses/>.

    To contact the author, send an email to calderonchristian73 AT gmail DOT com.
*/
#include "isprime.h"
#include <errno.h>
#include <sysexits.h>
#include <stdio.h>
#include <string.h>

#define BASE 10
#define VERSION_MAJOR 1
#define VERSION_MINOR 1
#define VERSION_FIX 1


void
print_error(const char *error){
  const char *default_msg = "\nUse --help to see usage details.\n";
  fwrite(error, strlen(error), 1, stderr);
  fwrite(default_msg, strlen(default_msg), 1, stderr);
}


int
usage()
{
  puts("Usage: isprime N");
  puts("       isprime --version");
  puts("       isprime ( -h | --help )");
  puts("");
  puts("Checks if N is a prime number.");
  puts("If N is a prime, 0 is returned.");
  puts("If N is not a prime, 1 is returned.");
  puts("If N is a negative integer then isprime checks 2^64 + N.");
  puts("");
  puts("Options:");
  puts("--version       Prints version info and exits.");
  puts("-h, --help      Prints this help info and exits.");
  return 0;
}


int
main(int argc, char **argv)
{
  
  if(argc != 2){
    print_error("Invalid argument count.");
    return EX_USAGE;
  }

  const char *help_opt = "-h";
  const char *help_opt_long = "--help";
  const char *version_opt_long = "--version";
  
  if(strcmp(argv[1], help_opt) == 0)
    return usage();

  if(strcmp(argv[1], help_opt_long) == 0)
    return usage();

  if(strcmp(argv[1], version_opt_long) == 0){
    printf("isprime v%d.%d.%d\n", VERSION_MAJOR, VERSION_MINOR, VERSION_FIX);
    puts("Copyright (C) 2020 Christian Calderon");
    puts("License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.");
    puts("This is free software: you are free to change and redistribute it.");
    puts("There is NO WARRANTY, to the extent permitted by law.");
    return 0;
  }

  uint64_t n = strtoull(argv[1], NULL, BASE);
  
  if((n == 0ULL && strcmp(argv[1], "0") == 0) || errno == ERANGE){
    print_error("Unable to parse argument as a base-10 64-bit int.");
    return EX_DATAERR;
  }

  if(isprime(n))
    return 0;
  else
    return 1;
}
