#!/usr/bin/env bash
# N = 2**61 - 1, the largest Mersenne prime number that fits in 64 bits.
declare -r N=2305843009213693951
declare -i i=0
declare -i LOOPS=100
declare -i START
declare -i END

echo "Benchmarking..."
START=$SECONDS
for (( i=0; i<LOOPS; i++ ))
do
    ./isprime $N
done

END=$SECONDS
AVG=`python3 -c "print('{:.3f}'.format( ( $END - $START ) / $LOOPS ))"`

echo "$LOOPS runs, average runtime: $AVG seconds."
