/*
    This file is part of isprime.

    isprime is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    isprime is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with isprime.  If not, see <https://www.gnu.org/licenses/>.

    To contact the author, send an email to calderonchristian73 AT gmail DOT com.
*/
#include "isprime.h"


typedef struct {
  uint64_t n;
  uint64_t start;
  uint64_t step;
  bool factor_found;
} worker_args;


void *
isprime_worker(void *args)
{
  worker_args *real_args = args;
  uint64_t n = real_args->n;
  uint64_t start = real_args->start;
  uint64_t step = real_args->step;
  uint64_t limit = sqrt(n);
  uint64_t odd;
  bool factor_found = false;

  for(odd = start; odd <= limit; odd += step){
    factor_found |= (n%odd == 0);
  }
  
  real_args->factor_found = factor_found;
  return NULL;
}


bool
isprime(uint64_t n)
{
  if(n < 2)
    return false;

  if(n == 2 || n == 3)
    return true;

  if(n%6 != 1 && n%6 != 5)
    return false;

  long cpu_count = sysconf(_SC_NPROCESSORS_CONF);
  size_t thread_count = (cpu_count > 2 ? cpu_count : 2);
  size_t minus_one_thread_count = thread_count / 2;
  size_t plus_one_thread_count = thread_count - minus_one_thread_count;
  pthread_t *threads = malloc(thread_count * sizeof *threads);
  worker_args *args = malloc(thread_count * sizeof *args);
  
  size_t i;
  for(i = 0; i < minus_one_thread_count; i++){
    args[i].n = n;
    args[i].start = 6*(i + 1) - 1;
    args[i].step = 6*minus_one_thread_count;
    args[i].factor_found = false;
  }
  
  for(; i < thread_count; i++){
    args[i].n = n;
    args[i].start = 6*(i + 1 - minus_one_thread_count) + 1;
    args[i].step = 6*plus_one_thread_count;
    args[i].factor_found = false;
  }

  for(i = 0; i < thread_count; i++)
    pthread_create(threads + i, NULL, &isprime_worker, args + i);

  for(i = 0; i < thread_count; i++)
    pthread_join(threads[i], NULL);

  bool factor_found = false;
  for(i = 0; i < thread_count; i++)
    factor_found |= args[i].factor_found;

  free(args);
  free(threads);
  return !factor_found;
}
